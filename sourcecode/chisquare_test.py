import numpy as np
import scipy.stats as ss

def get_frequencies(data):   
  """data - 1d array of uint8"""
  values, counts=np.unique(data, return_counts=True)
  frequencies = np.zeros(256, dtype=np.int)
  frequencies[values] = counts
  return frequencies

def chisquare_uniform_test(data):
  """data - 1d array of uint8"""
  frequencies = get_frequencies(data)
  return ss.chisquare(frequencies)
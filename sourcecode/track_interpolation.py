import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

def read_track(track_path):
  track = pd.read_csv(
            track_path,
            names = ['EventType','Timestamp','x', 'y'])
            
  t = track.Timestamp.to_numpy()
  xy = track[['x','y']].to_numpy()
  t -= t[0]
  xy[:,0] -= xy[0,0]
  xy[:,1] -= xy[0,1]
  return (t, xy)

def equalize_steps(t, xy, step = None):
  t_dedup, xy_dedup = _deduplicate_time(t, xy)
  return _equalize_steps(
    t_dedup, 
    xy_dedup, 
    step=step)

def _deduplicate_time(t, xy):
  idx = np.argwhere(t[1:]==t[:-1])
  if idx.shape[0] > 0:
    return \
      (np.delete(t, idx),
       np.delete(xy, idx, axis=0))
  else:
    return t, xy

def _equalize_steps(t, xy, step = None):
  if not step:
    dt = t[1:] - t[:-1]
    step = get_step(dt)

  tmin = np.float(t[0])
  tmax = np.float(t[-1])
  nsteps = \
    np.floor((tmax-tmin)/step)\
    .astype(np.int)

  t_new = \
    tmin + np.arange(0,nsteps+1)*step

  xy_new = \
    np.empty(
      (t_new.shape[0],2), 
      dtype=np.int)
  xy_new[:,0] = _interpolate(t, xy[:,0], t_new)
  xy_new[:,1] = _interpolate(t, xy[:,1], t_new)

  t_new_int = np.rint(t_new).astype(np.int64)

  return (t_new_int, xy_new)

def _interpolate(t_old, x, t_new):
  interpolant = \
    interp1d(
      t_old,x,
      kind='cubic',
      assume_sorted=True,
      copy=False)
  return np.rint(interpolant(t_new))\
    .astype(np.int)

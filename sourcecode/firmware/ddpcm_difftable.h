#ifndef HEADER_GUARD_DDPCM_DIFFTABLE
#define HEADER_GUARD_DDPCM_DIFFTABLE

const PROGMEM int8_t DDPCM_DELTA[16] = {
    -16, -12, -8, -6, -4, -3, -2, -1,
    1, 2, 3, 4, 6, 8, 12, 16,
};

#define DDPCM_DELTA_LENGTH (sizeof(DDPCM_DELTA))

#endif

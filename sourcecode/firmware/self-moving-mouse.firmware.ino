#include "eeprom.h"

#include <DigiMouse.h>
#include "tracks-library.h"
#include "ddpcm_difftable.h"

#include "microrng.h"
#include "debouncing.h"

#define DELAY(T) DigiMouse.delay(T)
#define INIT_USB() DigiMouse.begin()
#define MOUSE_MOVE(dx, dy) DigiMouse.move(dx, dy, 0)

#include "track-utils.h"

#define N_TRACKS (sizeof(track_sizes))

#define EEPROM_VERSION 2
#define EEPROM_ADDR_VERSION               (uint8_t*)0
#define EEPROM_ADDR_MIN_DELAY             (uint8_t*)1
#define EEPROM_ADDR_DELAY_INTERVAL_LENGTH (uint8_t*)2

#define DEFAULT_DELAY_MIN_DELAY       2
#define DEFAULT_DELAY_INTERVAL_LENGTH 4
#define DELAY_BOUNDARY_INCREMENT_MS   5000L

#define DEBOUNCING_THRESHOLD       20L
#define LONG_BUTTON_PRESS_DURATION 3000L

#define TRACK_TSTEP_MS 8
#define DELAY_TSTEP_MS 10L
#define BLINK_LENGTH   1000L

#define PIN_LED 1
#define PIN_BTN 2

// 2   in documentation
#define DEVICE_MODE_STOP             0
// 3.1 in documentation
#define DEVICE_MODE_PLAYING_TRACK    1
// 3.2 in documentation
#define DEVICE_MODE_TRACK_DELAY      2
// 4.1 in documentation
#define DEVICE_MODE_CONFIG_MIN_DELAY 3
// 4.2 in documentation
#define DEVICE_MODE_CONFIG_MAX_DELAY 4

static RNGState rngState;

static PinState button1;
static uint8_t btnPressCounter;
static uint8_t blinkCounter;
static uint8_t previousTrackId = 0;

static TrackPlayerState trackPlayerState;
static uint32_t delayEndTime = 0;
static uint8_t deviceMode = DEVICE_MODE_STOP;

// Minimum delay length measured in
// DELAY_BOUNDARY_INCREMENT_MS increments
static uint8_t minDelay;
// Delay interval length measured in
// DELAY_BOUNDARY_INCREMENT_MS increments
static uint8_t delayIntervalLength;

void setup() {
  setupWatchdogTimer();

  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_BTN, INPUT);

  restoreDelaysFromEEPROM();

  UpdatePinState(
    &button1,
    digitalRead(PIN_BTN),
    millis());
  MarkStateAsProcessed(&button1);
  attachInterrupt(0, onBtnChange, CHANGE);

  INIT_USB();
}

void loop() {
    uint32_t currentTime = millis();
    // extracting button state changes processing
    // to separate function would cost 
    // 8 bytes of storage
    int8_t button1Event =
        GetUnprocessedEvent(
          &button1,
          DEBOUNCING_THRESHOLD,
          LONG_BUTTON_PRESS_DURATION,
          currentTime);
    if(button1Event!=NO_UNPROCESSED_EVENTS)
    {
      MarkStateAsProcessed(&button1);
    }

    switch (deviceMode){
        case DEVICE_MODE_STOP:
            if(button1Event == LONG_BUTTON_PRESS)
            {
              btnPressCounter = 0;
              blinkCounter = minDelay;
              delayEndTime = 0;
              deviceMode = DEVICE_MODE_CONFIG_MIN_DELAY;
            }
            else if(button1Event == SHORT_BUTTON_PRESS)
            {
              switchState_PlayTrack();
            }
            break;
        case DEVICE_MODE_CONFIG_MIN_DELAY:
            blink(currentTime);
            if(button1Event == LONG_BUTTON_PRESS)
            {
              if(btnPressCounter>0)
              {
                minDelay = btnPressCounter;
              }
              btnPressCounter = 0;
              blinkCounter = delayIntervalLength;
              deviceMode = DEVICE_MODE_CONFIG_MAX_DELAY;
            }
            else if(button1Event == SHORT_BUTTON_PRESS)
            {
              if(btnPressCounter<255)
              {
                btnPressCounter++;
              }
            }
            break;
        case DEVICE_MODE_CONFIG_MAX_DELAY:
            blink(currentTime);
            if(button1Event == LONG_BUTTON_PRESS)
            {
              if(btnPressCounter>0)
              {
                delayIntervalLength = btnPressCounter;
              }
              saveDelaysToEEPROM();
              switchState_Stop();
            }
            else if(button1Event == SHORT_BUTTON_PRESS)
            {
              if(btnPressCounter<255)
              {
                btnPressCounter++;
              }
            }
            break;
        case DEVICE_MODE_PLAYING_TRACK:
            if(button1Event >= SHORT_BUTTON_PRESS){
              switchState_Stop();
            }
            else
            {
              if(playNextStep(&trackPlayerState)){
                  DELAY(TRACK_TSTEP_MS);
                  return;
              }
              else{
                  switchState_Delay(currentTime);
              }
            }
            break;
        case DEVICE_MODE_TRACK_DELAY:
            if(button1Event >= SHORT_BUTTON_PRESS){
              switchState_Stop();
            }
            else
            {
              if(currentTime>=delayEndTime){
                  switchState_PlayTrack();
              }
            }
            break;
    }
    // Some delay is necessary for
    // software USB mouse or serial port
    // to function properly
    DELAY(DELAY_TSTEP_MS);
    updateRandomSample(&rngState);
}

void blink(uint32_t currentTime) {
  if(btnPressCounter==0 &&
     blinkCounter>0 &&
     currentTime>=delayEndTime)
  {
    uint8_t nextLEDState = digitalRead(PIN_LED)?LOW:HIGH;
    if(nextLEDState)
    {
      blinkCounter--;
    }
    digitalWrite(PIN_LED, nextLEDState);
    delayEndTime = currentTime+BLINK_LENGTH;
  }
  else
  {
    digitalWrite(PIN_LED, LOW);
  }
}

inline void restoreDelaysFromEEPROM() {
  uint8_t currentVersion = 
    eeprom_read_byte(EEPROM_ADDR_VERSION);
  if(currentVersion == EEPROM_VERSION)
  {
    minDelay = eeprom_read_byte(EEPROM_ADDR_MIN_DELAY);
    delayIntervalLength =
      eeprom_read_byte(EEPROM_ADDR_DELAY_INTERVAL_LENGTH);
  }
  else
  {
    // EEPROM version differs
    // using default values
    minDelay = DEFAULT_DELAY_MIN_DELAY;
    delayIntervalLength = DEFAULT_DELAY_INTERVAL_LENGTH;

    // EEPROM not updated here
    // to reduce code size and to conserve
    // write cycles resource.
    // Wrong version at EEPROM_ADDR_VERSION
    // has the same effect as default values.
  }
}

inline void saveDelaysToEEPROM(){
  // eeprom_update_byte(..) only updates EEPROM
  // if current value does not match
  // new value to converve write cycles resource
  eeprom_update_byte(
    EEPROM_ADDR_VERSION,
    EEPROM_VERSION);
  eeprom_update_byte(
    EEPROM_ADDR_MIN_DELAY,
    minDelay);
  eeprom_update_byte(
    EEPROM_ADDR_DELAY_INTERVAL_LENGTH,
    delayIntervalLength);
}

void switchState_Stop(){
  deviceMode = DEVICE_MODE_STOP;
  digitalWrite(PIN_LED, LOW);
}

void switchState_PlayTrack(){
    // https://stackoverflow.com/a/15734524
    // Previous track number is incremented
    // at least one and at most N-1 tracks
    // together with final mod N operation
    // this guarantees that no two consecutive
    // number will be the same.
    // This method uses two slow divisions
    // but for our purposes it does not matter.
    uint8_t track_id =
      (previousTrackId +
       (nextRandomNumber() % (N_TRACKS-1))
       +1) % N_TRACKS;
    setTrack(
      &trackPlayerState,
      tracks[track_id],
      track_sizes[track_id]);
    digitalWrite(PIN_LED, HIGH);
    deviceMode = DEVICE_MODE_PLAYING_TRACK;
}

void switchState_Delay(uint32_t currentTime){
    delayEndTime =
        currentTime +
        DELAY_BOUNDARY_INCREMENT_MS * (uint32_t)minDelay +
        ((DELAY_BOUNDARY_INCREMENT_MS *
         (uint32_t)delayIntervalLength *
         (uint32_t)nextRandomNumber()) / 255);

    deviceMode = DEVICE_MODE_TRACK_DELAY;
}

void onBtnChange(){
  UpdatePinState(
    &button1,
    digitalRead(PIN_BTN),
    millis());
}

uint8_t nextRandomNumber(){
  while(!updateRandomSample(&rngState)){
  }
  return useRandomNumber(&rngState);
}

inline void setupWatchdogTimer() {
  cli();
  MCUSR = 0;

  // Start timed sequence
  WDTCR |= _BV(WDCE) | _BV(WDE);

  // Put WDT into interrupt mode
  // Set shortest prescaler(time-out) 
  // value = 2048 cycles (~16 ms)
  WDTCR = _BV(WDIE);

  sei();
}

ISR(WDT_vect)
{
  RNGOnWatchdog(&rngState);
}

#ifndef HEADER_GUARD_TRACKS_LIBRARY
#define HEADER_GUARD_TRACKS_LIBRARY

#include "track070.h"
#include "track073.h"
#include "track151.h"
#include "track153.h"
#include "track155.h"
#include "track157.h"

static PGM_VOID_P tracks[] = {
    &TRACK070_DDPCM, 
    &TRACK073_DDPCM,
    &TRACK151_DDPCM,
    &TRACK153_DDPCM,
    &TRACK155_DDPCM,
    &TRACK157_DDPCM
    };

static const uint8_t track_sizes[] = {
    TRACK070_LENGTH, 
    TRACK073_LENGTH,
    TRACK151_LENGTH,
    TRACK153_LENGTH,
    TRACK155_LENGTH,
    TRACK157_LENGTH
  };

#endif

#ifndef HEADER_GUARD_TRACK_UTILS
#define HEADER_GUARD_TRACK_UTILS

// This library needs folowing macros to be defined
// TRACK_TSTEP_MS   - target mouse report period 
//                    in milliseconds (time between 
//                    mouse steps)
// DELAY(T)         - pauses execution for T milliseconds
// MOUSE_MOVE(x, y) - sends mouse movement notification to
//                    host and/or prints debug messages
// it also assumes that
// DDPCM_DELTA is difference table for DDPCM comression 
// in progmem

typedef struct TrackPlayerState {
    PGM_VOID_P track;
    uint8_t track_size;
    uint8_t current_step;
    int8_t current_dx;
    int8_t current_dy;
} TrackPlayerState;

void setTrack(
    TrackPlayerState* state, 
    PGM_VOID_P track, 
    uint8_t track_length){

    state->track = track;
    state->track_size = track_length; 
    state->current_dx = 
      (int8_t)pgm_read_byte(state->track);
    state->current_dy = 
      (int8_t)pgm_read_byte(state->track + 1);
    state->current_step = 0;
}

// Plays next track step (if any).
// Returns false if end of track is reached
bool playNextStep(TrackPlayerState* state){    
    MOUSE_MOVE(state->current_dx, state->current_dy);

    if((state->current_step) >= (state->track_size)){
      return false;
    }
    
    uint8_t acceleration_codes =
      pgm_read_byte(state->track + state->current_step+2);

    uint8_t acceleration_x_idx = 
      acceleration_codes & 0x0F;    
    state->current_dx += 
      (int8_t)pgm_read_byte(DDPCM_DELTA+acceleration_x_idx);

    uint8_t acceleration_y_idx = 
      (acceleration_codes & 0xF0) >> 4;    
    state->current_dy += 
      (int8_t)pgm_read_byte(DDPCM_DELTA+acceleration_y_idx);
              
    state->current_step++;
    return true;
}

#endif

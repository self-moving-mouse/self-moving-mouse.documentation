#ifndef HEADER_GUARD_MICRO_RNG
#define HEADER_GUARD_MICRO_RNG

// Random number generator was adapted from
// https://gist.github.com/endolith/2568571
// by endolith (https://gist.github.com/endolith)
//
// The gist of the algorithm is using
// frequency and phase mismatch between two independent
// clocks: main clock of the controller and watchdog
// timer to gather randomness. Circular shifting and
// xor-ing distributes randomness between all bits of the
// result.

typedef struct RNGState {
    volatile uint8_t sample;
    volatile boolean sample_waiting;
    uint8_t current_bit;
    uint8_t random_number;
} RNGState;

// this procedure should be called from
// watchdog interrupt handler
void RNGOnWatchdog(RNGState* rngState){
    if(!rngState->sample_waiting){
        // sample is 8 bit.
        // higher bits of millis() are ignored
        rngState->sample = millis();
        rngState->sample_waiting = true;
    }
}

// Rotate bits to the left
// https://en.wikipedia.org/wiki/Circular_shift
byte _rotl(const uint8_t value, uint16_t shift) {
  if ((shift &= sizeof(value)*8 - 1) == 0){
    return value;
  }
  return (value << shift) | 
         (value >> (sizeof(value)*8 - shift));
}

// This method should be called preferably at
// non-deterministic intervals to accumulate randomness.
// Each call adds 1 bit of randomness.
// Returns true when next random number is ready.
bool updateRandomSample(RNGState* rngState){
  if(rngState->current_bit>7){
    return true;
  }
  if(rngState->sample_waiting){
    rngState->sample_waiting = false;

    // Spreading randomness around
    rngState->random_number = 
      _rotl(rngState->random_number, 1);
    // XOR preserves randomness
    rngState->random_number ^= rngState->sample;

    rngState->current_bit++;
  }
  return false;
}

// returns random number and resumes
// randomness accumulation process.
uint8_t useRandomNumber(RNGState* rngState){
    uint8_t result = rngState->random_number;
    rngState->current_bit = 0;
    return result;
}
#endif

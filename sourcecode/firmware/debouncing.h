#ifndef DEBOUNCING_HEADER_GUARD
#define DEBOUNCING_HEADER_GUARD

// Debouncing strategy:
// 1. PinState->StateCurrent is updated on pin change 
//    interrupt with UpdatePinState(..) function. 
//    After each change PinState->lastChangeTime 
//    is set to current time.
// 2. State change is analyzed and acted upon in 
//    main loop. After state change has been processed 
//    PinState->StateOld should be set to 
//    PinState->StateCurrent (see MarkStateAsProcessed(..))

// Assumes the following pushbutton connection scheme
//        ^ VCC  
//        | 
//       | |
//       |R|
//       | |    SW 
//        |    __|__
//  pin --*-----   ------ GND
// 
// Short button press is registered when pin had been 
// LOW for less than longPressDuration, then became 
// HIGH and stayed that way for more than 
// debouncingThreshold
//
// Long button press is registered when pin had been 
// HIGH then became LOW and stayed that way for longer 
// than longPressDuration

#define NO_UNPROCESSED_EVENTS      0
#define SHORT_BUTTON_PRESS         1
#define LONG_BUTTON_PRESS          2

#define __NO_LONG_PRESS            0
#define __LONG_PRESS_NOT_PROCESSED 1
#define __LONG_PRESS_PROCESSED     2

typedef struct PinState {
    volatile uint8_t StateOld;
    volatile uint8_t StateCurrent;
    volatile uint32_t lastChangeTime;
    uint8_t longPressStatus;
} PinState;


void UpdatePinState(
  PinState* pinState, 
  uint8_t newValue, 
  uint32_t currentTime)
{
    pinState->StateCurrent = newValue;
    if(pinState->StateOld != newValue)
    {
        pinState->lastChangeTime = currentTime;
    }
}

int8_t GetUnprocessedEvent(
  PinState* pinState, 
  uint32_t debouncingThreshold,
  uint32_t longPressDuration,
  uint32_t currentTime)
{     
    uint32_t timeSinceLastChange = 
      currentTime - pinState->lastChangeTime;    
    if(timeSinceLastChange >= debouncingThreshold) 
    {
      if(pinState->StateOld)
      {
        if(!pinState->StateCurrent)
        {
          // was UP and now DOWN          
          pinState->longPressStatus = __NO_LONG_PRESS;          
          pinState->StateOld = pinState->StateCurrent;            
        }
      }
      else
      {
        if(pinState->StateCurrent)
        {   
          // was DOWN and now UP
          if(pinState->longPressStatus)
          {
            pinState->StateOld = pinState->StateCurrent;              
          }
          else
          {
            return SHORT_BUTTON_PRESS;
          }
        }
        else
        {
          // was DOWN and now DOWN
          if((timeSinceLastChange >= longPressDuration) &&
             (pinState->longPressStatus != 
                  __LONG_PRESS_PROCESSED))
          {
            pinState->longPressStatus = 
                  __LONG_PRESS_NOT_PROCESSED;
            return LONG_BUTTON_PRESS;            
          }
        }
      }
    }
          
    return NO_UNPROCESSED_EVENTS;
}

void MarkStateAsProcessed(PinState* pinState)
{
    pinState->StateOld = pinState->StateCurrent;
    // Only "forgetting" about long press after
    // UP edge (long or short) so that
    // longPress is not reset when user code
    // marks long press as processed
    if(pinState->longPressStatus==__LONG_PRESS_NOT_PROCESSED)
    {
      pinState->longPressStatus = __LONG_PRESS_PROCESSED;
    }
}
#endif

#include <stdio.h>
#include <X11/Xlib.h>

int main(int argc, char **argv) {
    Display *display;
    XEvent xevent;
    Window window;

    if( (display = XOpenDisplay(NULL)) == NULL ) {
        return -1;
    }

    window = DefaultRootWindow(display);
    XAllowEvents(display, AsyncBoth, CurrentTime);

    XGrabPointer(
        display,
        window,
        1,
        PointerMotionMask,
        GrabModeAsync,
        GrabModeAsync,
        None,
        None,
        CurrentTime);

    while(1) {
        XNextEvent(display, &xevent);

        if(xevent.type == MotionNotify) {
          printf(
           "MV, %ld, %d, %d\n",
            xevent.xmotion.time,
            xevent.xmotion.x_root,
            xevent.xmotion.y_root);
          fflush(stdout);
        }
    }
    
    return 0;
}
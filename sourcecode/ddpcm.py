import numpy as np

class DDPCM:
  def __init__(self, diff_table):
    self.diff_table = \
       np.array(diff_table, dtype=np.int)

  def __encode_1d(self, input_buf, dtype):
    output_buf = \
      np.empty(input_buf.shape[0] - 2,
               dtype)
    prediction = input_buf[1]
    prediction_dx  = input_buf[1] - input_buf[0]
    for i, input_sample in enumerate(input_buf[2:]):
      predictions = prediction + prediction_dx + \
                    self.diff_table
      abs_error = np.abs(predictions - input_sample)
      diff_index = np.argmin(abs_error)
      output_buf[i] = diff_index
      prediction_dx += self.diff_table[diff_index]
      prediction += prediction_dx
    return output_buf

  def __decode_1d(self, input_buf, first_sample,
          first_delta, dtype):

    differences = self.diff_table[input_buf]
    result = np.empty(input_buf.shape[0]+2, dtype=dtype)
    result[0] = first_sample
    result[1] = first_sample + first_delta
    result[2:] = \
      result[1] + \
      np.cumsum(first_delta +
        np.cumsum(differences, dtype=dtype))
    return result

  def encode(self, input_buf, symbols_dtype = np.uint):    
    if len(input_buf.shape)==1:
      return self.__encode_1d(input_buf, symbols_dtype)
    else:
      return \
        np.stack(
          [self.__encode_1d(input_buf[:,i], symbols_dtype)\
          for i in range(input_buf.shape[1])],
          axis=1)

  def decode(self, input_buf, first_sample=None,
         first_delta=None, result_dtype=np.double):

    if len(input_buf.shape)==1:
      return self.__decode_1d(
        input_buf,
        first_sample if first_sample else 0,
        first_delta if first_delta else 0,
        result_dtype)
    else:      
      return \
        np.stack(
          [self.__decode_1d(
            input_buf[:,i],
            0 if first_sample is None else first_sample[i],
            0 if first_delta is None else first_delta[i],
            result_dtype) \
           for i in range(input_buf.shape[1])],
          axis=1)

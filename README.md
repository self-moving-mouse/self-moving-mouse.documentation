This repository contains LaTeX source code, images and data for user manual and technical description of Self-Moving Mouse project (for now only in russian).

See also
- [firmware](https://gitlab.com/fedorkotov/self-moving-mouse.firmware) repository
- [auxiliary tools](https://gitlab.com/fedorkotov/self-moving-mouse.tools) repository

## Build instructions

`self-moving-mouse-manual.tex` is the main file of the project.

I used LuaLaTeX v1.10.0 from TeXLive 2019 distribution and TeXstudio on Ubuntu Linux 20.04 to build this document. 

I also tested it with MiKTeX 20.12 on Windows. To achieve successful compilation I had  
- to install `ccicons` and `tikz-uml` packages manually.
- to add `\epstopdfDeclareGraphicsRule` command for eps images to be transformed into pdf correctly

Any other modern LuaLaTeX should do but I haven't checked.

Currently document structure is not optimized for fast builds. Compilation take several minutes on my machine.

## Legal Info

Files in this repository and PDF files that can be downloaded from [Releases](https://gitlab.com/fedorkotov/self-moving-mouse.documentation/-/releases) page are licensed under [CC-BY-NC-SA version 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license with the following exceptions:
- `illustrations/logo.svg` is licensed under [CC-BY-NC version 4.0](https://creativecommons.org/licenses/by-nc/4.0/). This image was taken from [flyclipart.com](https://flyclipart.com/animals-hamster-lab-mammal-mouse-pets-run-toy-wheel-icon-hamster-black-and-white-clipart-843513).
- contents of `sourcecode` folder except `sourcecode/firmware` subfolder are licensed under [MIT](https://opensource.org/licenses/MIT) license
- contents of `sourcecode/firmware` folder are licensed under [GNU GPL v2](https://opensource.org/licenses/GPL-2.0) license
- `LiberationMono-*.ttf` files (Liberation Mono font) are developed by Red Hat, Inc. and licensed under [SIL Open Font License, Version 1.1](http://scripts.sil.org/OFL)
